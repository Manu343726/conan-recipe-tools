from conans import ConanFile
from conan_common_recipes.cmake import *
from conan_common_recipes.headeronly import *
from conan_common_recipes.require_scm import *

class ConanCommonRecipes(ConanFile):
    name = "conan_common_recipes"
    version = "0.0.8"
    url = "https://gitlab.com/Manu343726/conan-common-recipes"
    license = "MIT"
    description = "Common recipes for conan.io packages"
    exports = "*.py"
